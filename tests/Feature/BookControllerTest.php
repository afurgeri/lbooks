<?php

namespace Tests\Feature;

use App\Models\BookModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BookControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function canGetEmptyList(): void
    {
        $response = $this->get('/api/books?page=1&total=10');
        $response->assertStatus(200);
        $response->assertSee('[]');
    }

    /**
     * @test
     */
    public function canGetBooks(): void
    {
        BookModel::factory()->count(20)->create();

        $response = $this->get('/api/books?page=2&total=10');
        $response->assertStatus(200);
        $this->assertEquals(20, $response->json('totalItems'));
        $this->assertEquals(2, $response->json('page'));
        $this->assertEquals(2, $response->json('totalPages'));
    }

    /**
     * @test
     */
    public function canCreateABook(): void
    {
        $book = BookModel::factory()->make()->toArray();
        $response = $this->post('/api/books', $book);
        $response->assertStatus(201);
        $this->assertEquals(1, $response->json('id'));
    }

    /**
     * @test
     */
    public function canCreateBookWithInvalidData(): void
    {
        $book = BookModel::factory()->make(['title' => '', 'publicationDate' => ''])->toArray();
        $response = $this->post('/api/books', $book);
        $response->assertStatus(400);
    }

    /**
     * @test
     */
    public function cantGetNonExistsBook(): void
    {
        $response = $this->get('/api/books/1000');
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function canGetABook(): void
    {
        $book = BookModel::factory()->create();
        $response = $this->get('/api/books/' . $book->id);
        $response->assertStatus(200);
        $this->assertEquals($book->id, $response->json('id'));
        $this->assertEquals($book->title, $response->json('title'));
        $this->assertEquals($book->author, $response->json('author'));
        $this->assertEquals($book->gender, $response->json('gender'));
        $this->assertEquals($book->publicationDate, $response->json('publicationDate'));
    }

    /**
     * @test
     */
    public function canUpdateABook(): void
    {
        $book = BookModel::factory()->create();
        $newDataBook = BookModel::factory()->make(['publicationDate' => (new \DateTime())->format("Y-m-d"), 'price'=>10.55]);
        $response = $this->put('/api/books/' . $book->id, $newDataBook->toArray());
        $response->assertStatus(200);
        $book->refresh();
        $this->assertEquals($book->id, $response->json('id'));
        $this->assertEquals($book->title, $response->json('title'));
        $this->assertEquals($book->author, $response->json('author'));
        $this->assertEquals($book->gender, $response->json('gender'));
        $this->assertEquals($book->publicationDate, $response->json('publicationDate'));
    }

    /**
     * @test
     */
    public function canDeleteABook(): void
    {
        $book = BookModel::factory()->create();
        $response = $this->delete('/api/books/' . $book->id);
        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function cantUpdateWithInvalidDate(): void
    {
        $book = BookModel::factory()->create();
        $newDataBook = BookModel::factory()->make(['publicationDate' => 'lalala']);
        $response = $this->put('/api/books/' . $book->id, $newDataBook->toArray());
        $response->assertStatus(400);
        $response->assertSee('errors');
    }

    /**
     * @test
     */
    public function canCreateOrUpdateWithInvalidBody(): void
    {
        $book = BookModel::factory()->create();
        $newDataBook = BookModel::factory()->make(['publicationDate' => '', 'title' => '', 'isbn' => '', 'price' => 'a', 'author' => '', 'gender' => '']);
        $response = $this->put('/api/books/' . $book->id, $newDataBook->toArray());
        $response->assertStatus(400);
        $response->assertSee('errors');
        $this->assertEquals(6, count($response->json('errors')));
    }
}
