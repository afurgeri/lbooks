<?php

namespace Tests\Feature\Models;

use App\Models\BookModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BookModelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function testBookModelHasProperties(): void
    {
        $book = BookModel::factory()->create();

        $this->assertIsString($book->isbn);
        $this->assertIsString($book->title);
        $this->assertIsString($book->author);
        $this->assertIsString($book->gender);
        $this->assertIsFloat($book->price);
        $this->assertIsString($book->publicationDate);

        $this->assertDatabaseHas(BookModel::class, $book->toArray());
    }

}
