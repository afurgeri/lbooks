<?php

namespace Tests\Feature\Console\Commands;

use App\Helper\BookHelper;
use App\Models\BookModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithConsoleEvents;
use Tests\TestCase;

class BookCommandTest extends TestCase
{
    use RefreshDatabase;
    use WithConsoleEvents;

    /**
     * @test
     */
    public function canGetBooksPerPage(): void
    {
        BookModel::factory()->count(10)->create();

        $this->artisan('books:list')
            ->expectsQuestion('Page number?', 1)
            ->expectsQuestion('Items per page?', 10)
            ->expectsTable(BookHelper::getFieldsWithId(), BookModel::all());
    }

    /**
     * @test
     */
    public function cantGetListWithInvalidParams(): void
    {
        BookModel::factory()->count(10)->create();

        $this->artisan('books:list')
            ->expectsQuestion('Page number?', "a")
            ->expectsQuestion('Items per page?', "b")
            ->expectsOutput("Invalid params");
    }

    /**
     * @test
     */
    public function canAddABook(): void
    {
        $this->artisan('books:add')
            ->expectsQuestion('isbn:', "abc123")
            ->expectsQuestion('title:', "title test")
            ->expectsQuestion('author:', "Dr Lorens")
            ->expectsQuestion('price:', 10.10)
            ->expectsQuestion('gender:', BookHelper::getRandomGender())
            ->expectsQuestion('publicationDate:', "2020-02-01")
            ->expectsOutput('Book created');

        $this->assertEquals(1, BookModel::count());
    }

    /**
     * @test
     */
    public function canUpdateABook(): void
    {
        $book = BookModel::factory()->create();
        $newTitle = "new Title";
        $this->artisan('books:update')
            ->expectsQuestion('book id?', $book->id)
            ->expectsConfirmation('Do you wish to change isbn? actual=' . $book->isbn, 'no')
            ->expectsConfirmation('Do you wish to change title? actual=' . $book->title, 'yes')
            ->expectsQuestion('title:', $newTitle)
            ->expectsConfirmation('Do you wish to change author? actual=' . $book->author, 'no')
            ->expectsConfirmation('Do you wish to change price? actual=' . $book->price, 'no')
            ->expectsConfirmation('Do you wish to change gender? actual=' . $book->gender, 'no')
            ->expectsConfirmation('Do you wish to change publicationDate? actual=' . $book->publicationDate, 'no')
            ->expectsOutput('Book updated');

        $this->assertEquals($newTitle, $book->refresh()->title);
    }

    /**
     * @test
     */
    public function canDeleteABook(): void
    {
        $book = BookModel::factory()->create();
        $this->artisan('books:delete')
            ->expectsQuestion('book id?', 1)
            ->expectsConfirmation('Do you wish to delete this book?', 'yes')
            ->expectsOutput('Book deleted');

        $this->assertEquals(0, BookModel::count());
    }

    /**
     * @test
     */
    public function canGetABook(): void
    {
        $book = BookModel::factory()->create();
        $this->artisan('books:get')
            ->expectsQuestion('book id?', 1)
            ->expectsOutputToContain($book->isbn);
    }
}
