<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Error500Test extends TestCase
{
    /**
     * @test
     */
    public function canGetEmptyList(): void
    {
        $response = $this->get('/api/books?page=1&total=10');
        $response->assertStatus(500);
        $response->assertSee('Internal Server Error');
        $response->assertDontSee("SQLSTATE");
    }
}
