<?php

namespace Tests\Unit;

use App\Exceptions\BookNotFound;
use App\Exceptions\InvalidBookException;
use App\Helper\BookHelper;
use App\Models\BookModel;
use App\Repository\BookRepository;
use App\System\SystemBook;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SystemBookTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return SystemBook
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function system(): SystemBook
    {
        return app()->make(SystemBook::class);
    }

    /**
     * @test
     */
    public function cantCreateAnInvalidBook(): void
    {
        try {
            $this->system()->addBook();
            $this->fail();
        } catch (InvalidBookException $e) {
            $this->assertEquals(SystemBook::INVALID_BOOK, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function cantCreateAnBookWithInvalidData(): void
    {
        try {
            $bookData = [];
            foreach (BookHelper::getFields() as $field) {
                $bookData[$field] = "";
            }
            $this->system()->addBook($bookData);
            $this->fail();
        } catch (InvalidBookException $e) {
            $this->assertEquals(SystemBook::INVALID_BOOK, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function canCreateABook(): void
    {
        $book = $this->system()->addBook(BookModel::factory()->make()->toArray());
        $this->assertDatabaseHas(BookModel::class, $book);
    }

    /**
     * @test
     */
    public function cantDuplicateABook(): void
    {
        $bookData = BookModel::factory()->make()->toArray();
        $this->system()->addBook($bookData);

        try {
            $this->system()->addBook($bookData);
            $this->fail();
        } catch (InvalidBookException $e) {
            $this->assertEquals(BookRepository::ALREADY_EXIST, $e->getMessage());
        }
        $this->assertEquals(1, BookModel::all()->count());
    }

    /**
     * @test
     */
    public function canCreateABookWithInvalidParams(): void
    {
        try {
            $this->system()->addBook(BookModel::factory()->make(['price'=>'á', 'publicationDate'=>'a', 'gender'=>'a'])->toArray());
            $this->fail();
        } catch (InvalidBookException $e) {
            $this->assertEquals(SystemBook::INVALID_BOOK, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function canUpdateABook(): void
    {
        $book = BookModel::factory()->create();
        $updatedBook = $this->system()->updateBook($book->id, BookModel::factory()->make()->toArray());
        $this->assertDatabaseHas(BookModel::class, $updatedBook);
        $this->assertEquals(1, BookModel::all()->count());
    }

    /**
     * @test
     */
    public function canUpdateBookWithInvalidValues(): void
    {
        $book = BookModel::factory()->create();
        try {
            $this->system()->updateBook($book->id, BookModel::factory()->make(['price'=>'á', 'publicationDate'=>'a', 'gender'=>'a'])->toArray());
            $this->fail();
        } catch (InvalidBookException $e) {
            $this->assertEquals(SystemBook::INVALID_BOOK, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function cantUpdateNonExistsBook(): void
    {
        try {
            $this->system()->updateBook(1000, BookModel::factory()->make()->toArray());
            $this->fail();
        } catch (BookNotFound $e) {
            $this->assertEquals(BookRepository::BOOK_NOT_FOUND, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function canDeleteABook(): void
    {
        $book = BookModel::factory()->create();
        $this->system()->deleteBook($book->id);
        $this->assertDatabaseEmpty(BookModel::class);
    }

    /**
     * @test
     */
    public function cantDeleteANonExistsBook(): void
    {
        try {
            $this->system()->deleteBook(1000);
            $this->fail();
        } catch (BookNotFound $e) {
            $this->assertEquals(BookRepository::BOOK_NOT_FOUND, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function cantGetANonExistsBook(): void
    {
        try {
            $this->system()->getBook('1000');
            $this->fail();
        } catch (BookNotFound $e) {
            $this->assertEquals(BookRepository::BOOK_NOT_FOUND, $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function canGetABook(): void
    {
        $book = BookModel::factory()->create();
        $findedBook = $this->system()->getBook($book->id);
        $this->assertDatabaseHas(BookModel::class, $findedBook);
    }

    /**
     * @test
     */
    public function canGetPaginatedEmptyList(): void
    {
        $paginatedList = $this->system()->getPaginatedBookLists(1, 10);
        $this->assertEquals(0, $paginatedList['totalItems']);
        $this->assertEquals(1, $paginatedList['page']);
        $this->assertEquals(1, $paginatedList['totalPages']);
        $this->assertEquals(0, count($paginatedList['data']));
    }

    /**
     * @test
     */
    public function canGetPaginatedList(): void
    {
        BookModel::factory()->count(30)->create();

        $paginatedList = $this->system()->getPaginatedBookLists(2, 10);
        $this->assertEquals(30, $paginatedList['totalItems']);
        $this->assertEquals(2, $paginatedList['page']);
        $this->assertEquals(3, $paginatedList['totalPages']);
        $this->assertEquals(10, count($paginatedList['data']));
        $this->assertEquals(11, $paginatedList['data'][0]['id']);
    }

}
