<?php

namespace App\Helper;

class BookHelper
{

    const GENDERS = ['Misterio','Ciencia ficción','Romance','Drama','Aventura','Acción','Terror','Histórico','Biografía','Filosofía','Política','Negocios','Autoayuda'];
    const BOOK_FIELDS = ['isbn', 'title', 'author', 'price', 'gender', 'publicationDate'];

    public static function getGenders()
    {
        return self::GENDERS;
    }

    public static function getRandomGender()
    {
        return self::getGenders()[rand(0, count(self::GENDERS) - 1)];
    }

    public static function getFields()
    {
        return self::BOOK_FIELDS;
    }

    public static function rules()
    {
        return [
            'isbn' => 'string|required|max:50',
            'title' => 'string|required|max:50',
            'author' => 'string|required|max:50',
            'gender' => 'required|in:' . join(',', self::GENDERS),
            'publicationDate' => 'date|required',
            'price' => 'numeric'
        ];
    }

    public static function getFieldsWithId()
    {
        return array_merge(['id'], self::BOOK_FIELDS);
    }
}
