<?php

namespace App\Repository;

use App\Exceptions\BookNotFound;
use App\Exceptions\InvalidBookException;
use App\Models\BookModel;
use App\System\SystemBook;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Database\UniqueConstraintViolationException;
use Illuminate\Support\Collection;

class BookRepository
{
    const ALREADY_EXIST = "Already exists a book with this isbn";
    const BOOK_NOT_FOUND = "Book not found";

    public function save(array $bookInfo): array
    {
        return $this->fillAndPersistBook(new BookModel(), $bookInfo);
    }

    public function find(int $id)
    {
        return $this->findBookOrFail($id)->toArray();
    }

    public function update(int $id, array $bookInfo)
    {
        $findedBook = $this->findBookOrFail($id);
        return $this->fillAndPersistBook($findedBook, $bookInfo);
    }

    public function delete(mixed $id)
    {
        $this->findBookOrFail($id)->delete();
    }

    public function getPaginatedBookLists(int $page, int $itemsPerPage)
    {
        $paginator = BookModel::paginate($itemsPerPage, ['*'], 'page', $page);
        return [
            'totalItems' => $paginator->total(),
            'page' => $paginator->currentPage(),
            'totalPages' => $paginator->lastPage(),
            'data' => Collection::make($paginator->items())
        ];
    }

    /**
     * @param BookModel $book
     * @param array $bookParams
     * @return array
     * @throws InvalidBookException
     */
    private function fillAndPersistBook(BookModel $book, array $bookParams): array
    {
        try {
            $book->fill($bookParams);
            $book->save();
            return $book->toArray();
        } catch (UniqueConstraintViolationException $e) {
            throw new InvalidBookException(self::ALREADY_EXIST);
        } catch (QueryException $e) {
            throw new InvalidBookException(SystemBook::INVALID_BOOK);
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws BookNotFound
     */
    private function findBookOrFail(int $id): BookModel
    {
        try {
            $findedBook = BookModel::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new BookNotFound(self::BOOK_NOT_FOUND);
        }
        return $findedBook;
    }

}
