<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (InvalidBookException $e, Request $request) {
            return response()->json($e->getMessage(), 400);
        });
        $this->renderable(function (BookNotFound $e, Request $request) {
            return response()->json($e->getMessage(), 404);
        });
        $this->renderable(function (\Exception $e, Request $request) {
            return response()->json("Internal Server Error", 500);
        });
        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
