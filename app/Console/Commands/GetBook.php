<?php

namespace App\Console\Commands;

use App\Helper\BookHelper;
use App\System\SystemBook;
use Illuminate\Console\Command;

class GetBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'you can get books with this command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /**
         * @var $system SystemBook
         */
        $system = app()->make(SystemBook::class);
        $id = $this->ask("book id?");
        $book = $system->getBook($id);
        $item = [];
        foreach (BookHelper::getFieldsWithId() as $field) {
            $item[$field] = $book[$field];
        }
        $this->table(BookHelper::getFieldsWithId(), [$item]);
    }
}
