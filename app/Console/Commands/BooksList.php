<?php

namespace App\Console\Commands;

use App\Helper\BookHelper;
use App\Models\BookModel;
use App\System\SystemBook;
use Illuminate\Console\Command;
use Ramsey\Collection\Collection;

class BooksList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get books by page and limit';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $page = $this->ask('Page number?');
        $itemPerPages = $this->ask('Items per page?');
        if (!is_numeric($page) || !is_numeric($itemPerPages))
            return $this->error("Invalid params");

        /**
         * @var $system SystemBook
         */
        $system = app()->make(SystemBook::class);
        $paginatedBooks = $system->getPaginatedBookLists($page, $itemPerPages);
        $this->table(BookHelper::getFieldsWithId(), $paginatedBooks['data']);
    }
}
