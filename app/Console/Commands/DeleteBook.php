<?php

namespace App\Console\Commands;

use App\Helper\BookHelper;
use App\System\SystemBook;
use Illuminate\Console\Command;

class DeleteBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'you can delete books with this command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /**
         * @var $system SystemBook
         */
        $system = app()->make(SystemBook::class);
        $id = $this->ask("book id?");
        $book = $system->getBook($id);
        $item = [];
        foreach (BookHelper::getFieldsWithId() as $field) {
            $item[$field] = $book[$field];
        }
        $this->table(BookHelper::getFieldsWithId(), [$item]);
        if ($this->confirm("Do you wish to delete this book?" , false)) {
            $system->deleteBook($id);
        }
        $this->info("Book deleted");
    }
}
