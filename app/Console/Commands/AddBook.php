<?php

namespace App\Console\Commands;

use App\Helper\BookHelper;
use App\Models\BookModel;
use App\System\SystemBook;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class AddBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'you can add books with this command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = [];
        foreach (BookHelper::getFields() as $field) {
            $data[$field] = $this->ask("$field:");
        }
        /**
         * @var $system SystemBook
         */
        $system = app()->make(SystemBook::class);
        $book = $system->addBook($data);
        $item = [];
        foreach (BookHelper::getFieldsWithId() as $field){
            $item[$field] = $book[$field];
        }
        $this->info("Book created");
        $this->table(BookHelper::getFieldsWithId(), [$item]);
    }
}
