<?php

namespace App\Console\Commands;

use App\Helper\BookHelper;
use App\System\SystemBook;
use Illuminate\Console\Command;

class UpdateBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'you can update books with this command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /**
         * @var $system SystemBook
         */
        $system = app()->make(SystemBook::class);
        $id = $this->ask("book id?");
        $book = $system->getBook($id);
        foreach (BookHelper::getFields() as $field) {
            if ($this->confirm("Do you wish to change $field? actual=" . $book[$field], false)) {
                $book[$field] = $this->ask("$field:");
            }
        }
        $book = $system->updateBook($id, $book);
        $this->info("Book updated");
        $item = [];
        foreach (BookHelper::getFieldsWithId() as $field) {
            $item[$field] = $book[$field];
        }
        $this->table(BookHelper::getFieldsWithId(), [$item]);
    }
}
