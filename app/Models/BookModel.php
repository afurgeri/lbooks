<?php

namespace App\Models;

use App\Helper\BookHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookModel extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'books';
    public $fillable = BookHelper::BOOK_FIELDS;
}
