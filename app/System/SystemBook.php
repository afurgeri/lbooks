<?php

namespace App\System;

use App\Exceptions\InvalidBookException;
use App\Helper\BookHelper;
use App\Models\BookModel;
use App\Repository\BookRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

class SystemBook
{

    const INVALID_BOOK = "Book is invalid";

    private $bookRepository;

    /**
     * @throws BindingResolutionException
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function addBook($book = [])
    {
        $this->assertBookIsValid($book);
        return $this->bookRepository->save($book);
    }

    public function updateBook(int $id, array $book)
    {
        $this->assertBookIsValid($book);
        return $this->bookRepository->update($id, $book);
    }

    public function deleteBook(int $id)
    {
        $this->bookRepository->delete($id);
    }

    /**
     * @param mixed $book
     * @return void
     * @throws InvalidBookException
     */
    private function assertBookIsValid(array $book): void
    {
        foreach (BookHelper::getFields() as $field) {
            if (!array_key_exists($field, $book) || !$book[$field])
                throw new InvalidBookException(self::INVALID_BOOK);
        }
    }

    public function getBook(string $id)
    {
        return $this->bookRepository->find($id);
    }

    public function getPaginatedBookLists(int $page, int $itemsPerPage)
    {
        return $this->bookRepository->getPaginatedBookLists($page, $itemsPerPage);
    }
}
