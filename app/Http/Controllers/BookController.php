<?php

namespace App\Http\Controllers;

use App\System\SystemBook;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;


class BookController extends Controller
{
    private $system;

    public function __construct(SystemBook $systemBook)
    {
        $this->system = $systemBook;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $page = $request->get('page', 1);
        $total = $request->get('total', 10);
        return $this->system->getPaginatedBookLists($page, $total);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BookRequest $request)
    {
        $addedBook = $this->system->addBook($request->all());
        return response()->json($addedBook, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return response()->json($this->system->getBook($id));
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(BookRequest $request, string $id)
    {
        return response()->json($this->system->updateBook($id, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        return response()->json($this->system->deleteBook($id), 204);
    }
}
