<?php

namespace Database\Factories;

use App\Helper\BookHelper;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BookModel>
 */
class BookModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'isbn' => fake()->currencyCode . rand(0, 1000),
            'title' => fake()->text(15),
            'author' => fake()->firstName . ' ' . fake()->lastName,
            'price' => fake()->randomFloat(2, 10, 1000),
            'gender' => BookHelper::getRandomGender(),
            'publicationDate' => fake()->date()
        ];
    }
}
