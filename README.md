## LBOOKS - API CRUD de Ejemplo

Esta API CRUD de ejemplo tiene como objetivo cumplir con la siguiente consigna: 

[Consigna de Desarrollo_ Backend de gestión de libros (HTTP+CLI)](consigna.pdf)

### Requisitos previos: 
- PHP >=8.1
- Sqlite
- composer
- xdebug (coverage)

### Diseño planteado:
![img.png](img.png)

### Stack tecnológico
- Ide: Intellij IDEA.
- Db: Sqlite.
- PHP 8.2.10
- Framework: Laravel ^10.10
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

### Instalación: 
- Clonar el proyecto. 
- En caso de querer utilizar otra db, ajustar acceso en el archivo **.env**. 
- En caso de utilizar la configuración default con **sqlite**, crear el archivo **database.sqlite** en el directorio **database**.  
- Instalar dependencias con el comando **composer install**
- Crear/Actualizar esquema de base de datos con el comando **php artisan migrate**
- Ejecutar servidor web con el comando **php artisan serve**. Por default el puerto utilizado es el **8000**.

![Startup.gif](Startup.gif)

## Documentación de la API

### Obtener libros: 

- **Ruta:** `/api/books?page={number}&total={number}`
  - page: Número de página.
  - total: Cantidad de ítems por página.
- **Método:** GET
- **Parmámetros:**
- **Descripción:** Obtiene una lista paginada de libros.
- **Ejemplo de Solicitud:** `GET /api/books?page=1&total=2`
- **Respuesta Exitosa:** Código 200 OK
- **Ejemplo de Respuesta:**
  ```json
  {
    "totalItems": 50,
    "page": 1,
    "totalPages": 25,
    "data": [
        {
            "id": 1,
            "isbn": "GNF865",
            "title": "Distinctio ea.",
            "author": "Theresia Kshlerin",
            "price": 326.03,
            "gender": "Misterio",
            "publicationDate": "1976-12-02"
        },
        {
            "id": 2,
            "isbn": "NPR973",
            "title": "Qui.",
            "author": "Casimir Breitenberg",
            "price": 664.92,
            "gender": "Terror",
            "publicationDate": "1978-09-02"
        }
    ]
  }
  ```

### Crear un libro:

- **Ruta:** `/api/books`
- **Método:** POST
- **Descripción:** Permite crear un nuevo libro.
- **Ejemplo de Solicitud:** `POST /api/books`
  ```json
  {
    "isbn": "ABC123",
    "title": "El misterio de la dama",
    "author": "Dr Alex Furche",
    "price": 150.00,
    "publicationDate": "1985-07-23",
    "gender": "Misterio"
  }
- **Respuesta Exitosa:** Código 201 Created
- **Ejemplo de Respuesta:**
  ```json
  {
    "isbn": "ABC123",
    "title": "El misterio de la dama",
    "author": "Dr Alex Furche",
    "price": 150,
    "publicationDate": "1985-07-23",
    "gender": "Misterio",
    "id": 51
  }
  ```

### Actualizar un libro:

- **Ruta:** `/api/books/{id}`
  - id: Identificador del libro. 
- **Método:** PUT
- **Descripción:** Permite actualizar un libro existente.
- **Ejemplo de Solicitud:** `put /api/books/51`
  ```json
  {
    "isbn": "ABC123",
    "title": "El misterio de la dama nueva edición",
    "author": "Dr Alex Furche",
    "price": 150.00,
    "publicationDate": "1985-07-23",
    "gender": "Misterio"
  }
- **Respuesta Exitosa:** Código 200 OK
- **Ejemplo de Respuesta:**
  ```json
  {
    "id": 51,
    "isbn": "ABC123",
    "title": "El misterio de la dama nueva edición",
    "author": "Dr Alex Furche",
    "price": 150,
    "gender": "Misterio",
    "publicationDate": "1985-07-23"
  }
  ```
### Eliminar un libro:

- **Ruta:** `/api/books/{id}`
    - id: Identificador del libro.
- **Método:** DELETE
- **Descripción:** Permite eliminar un libro existente.
- **Ejemplo de Solicitud:** `delete /api/books/51`
- **Respuesta Exitosa:** Código 204 No Content

### Errores

- Código de estado 400: Solicitud incorrecta.
- Código de estado 404: Recurso no encontrado.
- Código de estado 500: Error interno del servidor.

## Interfaz CLI

Para acceder a la interfaz CLI se realiza por medio del comando **php artisan**.

Para ver las opciones disponibles ejecutar el comando **php artisan books**.

### Obtener listado de libros

- **Comando:** `books:list`
- **Descripción:** Permite visualizar una tabla de libros.
- **Ejemplo de ejecución:** `php artisan books:list`

![bookslistcli.gif](bookslistcli.gif)

### Obtener un libro

- **Comando:** `books:get`
- **Descripción:** Permite obtener un libro.
- **Ejemplo de ejecución:** `php artisan books:get`

![booksget.gif](booksget.gif)

### Agregar un libro

- **Comando:** `books:add`
- **Descripción:** Permite agregar un libro.
- **Ejemplo de ejecución:** `php artisan books:add`

![booksAdd.gif](booksAdd.gif)

### Actualizar un libro

- **Comando:** `books:update`
- **Descripción:** Permite actualizar un libro.
- **Ejemplo de ejecución:** `php artisan books:update`

![booksupdate.gif](booksupdate.gif)

### Eliminar un libro

- **Comando:** `books:delete`
- **Descripción:** Permite eliminar un libro.
- **Ejemplo de ejecución:** `php artisan books:delete`

![booksdeletecli.gif](booksdeletecli.gif)

## Precarga de libros

La precarga de libros se puede realizar por medio del comando `php artisan db:seed`

## Testing

El desarrollo del proyecto se realizó siguiendo la metodología **TDD** por lo que cuenta con una amplia cobertura de pruebas. 

### Ejecutar test
- **Comando:** `php artisan test`
- **Descripción:** Permite correr las pruebas del proyecto. 

![img_2.png](img_2.png)

### Ejecutar test con informe de cobertura
- **Comando:** `php artisan test --coverage`
- **Descripción:** Permite correr las pruebas del proyecto brindando además un informe de cobertura.

![img_1.png](img_1.png)
